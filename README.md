[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4686960.svg)](https://doi.org/10.5281/zenodo.4686960)

﻿# daily average temperature profile for NUTS2 regions for industrial sites



## Repository structure
Files:
```
data/hotmaps_task_2.7_temperature_profile_daily_avg_industry_yearlong_2010.csv  --daily average temperature profile for each NUTS-2 region weighted according to industrial site concentration
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration
```

## Documentation

This dataset shows the daily average temperature of the year 2010 on region level (NUTS2) for industrial sites. 
The European Climate Assessment & Dataset project [1] provides a dataset of the daily average temperature on tiles with a size of 25 x 25 km2. The temperature on each tile of the E-OBS dataset is weighted according to its share of industrial sites and the weight average of the NUTS2 region is calculated. 

The data will be used in the Hotmaps toolbox to generate temperature dependent heating and cooling demand profiles for industrial subsectors. 

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) section 2.8 page 121ff.



## References
[1] [European daily high-resolution gridded dataset of surface temperature and precipitation](doi:10.1029/2008JD10201) Haylock, M.R., N. Hofstra, A.M.G. Klein Tank, E.J. Klok, P.D. Jones, M. New. 2008, Geophys. Res., 113, D20119.

## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 


## Authors

Matthias Kuehnbach, Simon Marwitz, Anna-Lena Klingler <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://isi.fraunhofer.de/)
Fraunhofer ISI, Breslauer Str. 48, 
76139 Karlsruhe


## License


Copyright © 2016-2018: Matthias Kuehnbach, Anna-Lena Klingler, Simon Marwitz

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html



## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

“We acknowledge the E-OBS dataset from the EU-FP6 project ENSEMBLES
(http://ensembles-eu.metoffice.com) and the data providers in the ECA&D project
(http://www.ecad.eu).